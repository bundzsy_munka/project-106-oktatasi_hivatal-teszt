# Oktatási Hivatal
#### project-106-oktatasi_hivatal-teszt

Oktatási hivatal teszt feladat 2021.12.15

Hirdetés link:

https://www.profession.hu/allas/senior-php-fejleszto-oktatasi-hivatal-budapest-1764214?keyword=Oktat%25C3%25A1si%2BHivatal&hash=574c442ba932782cea285a2fa230b655&session

### Tesztelés előtti feladatok
Szükseges a `composer` inicializálása és minimum `php` 7.4.1* verzió.

Az alábbi parancs, telepíti a szükséges `PHPUnit` és egyéb könyvtárakat.
~~~ command
composer install
~~~
---
####Telepítés után futtatható a teszt az alábbi parancsal
input adatok alapján inicializált iskolák tesztelése
~~~ command
 php vendor/bin/phpunit test/School/SchoolTest.php
~~~
input adatok és visszatérésük alapján lefuttatott tesztek
~~~ command
  php vendor/bin/phpunit test/ScoringSystem/ScoringSystemTest.php
~~~

---
Továbbá futtatható a `CLI` ben a kód ami paraméterül opcionálisan adható értékek `0`,`1`,`2`,`3` ezek az input.php fájl const elemei. Alapértelmezés szerínt a 0 paramétert kapja meg, ha nem állít be semmit vagy rossz értéket állít be.

Alapból ..
~~~ command
 php src/Commands/ScoringSystemCommand.php
~~~
..vagy így akár
~~~ command
 php src/Commands/ScoringSystemCommand.php 3
~~~
---
Tesztelt környezet:
`php version 7.4.1`
`composer version 2.1.3  `
