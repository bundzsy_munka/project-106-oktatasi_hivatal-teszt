<?php

use \PHPUnit\Framework\TestCase;

require 'src\ScoringSystem.php';
require 'src\Controllers\AbstractBaseController.php';
require 'src\HelperTrait.php';
require 'src\Controllers\SchoolController.php';
require 'src\Models\AbstractModel.php';
require 'src\Models\School\SchoolModelInterface.php';
require 'src\Models\School\BaseSchoolModel.php';
require 'src\Models\School\SchoolModel.php';
require 'src\Models\Subject\SubjectInterface.php';
require 'src\Models\Subject\AbstractSubjectModel.php';
require 'input\HomeworkInput.php';
require 'src\Controllers\CompetitionController.php';
require 'src\Models\Competition\CompetitionModel.php';
require 'src\Models\Subject\RequiredSubjectModel.php';
require 'src\Models\Subject\OptionalSubjectModel.php';
require 'src\Models\ExtraCertificate\ExtraCertificateModel.php';
require 'src\Controllers\RequiredSubjectController.php';
require 'src\Controllers\OptionalSubjectController.php';
require 'src\Controllers\ExtraCertificatesController.php';
require 'src\Messages\Messages.php';
require 'src\Messages\ErrorMessage.php';


/**
 * Class CalculateTest
 *
 * RUN command:
 * php vendor/bin/phpunit test/ScoringSystem/ScoringSystemTest.php
 */
class ScoringSystemTest extends TestCase
{

	use HelperTrait;

	public const RESPONSE_TYPE_0 = 'output: 470 (370 alappont + 100 többletpont)';
	public const RESPONSE_TYPE_1 = 'output: 476 (376 alappont + 100 többletpont)';
	public const RESPONSE_TYPE_2 = 'output: hiba, nem lehetséges a pontszámítás a kötelező érettségi tárgyak hiánya miatt';
	public const RESPONSE_TYPE_3 = 'output: hiba, nem lehetséges a pontszámítás a magyar nyelv és irodalom tárgyból elért 20% alatti eredmény miatt';

	public function testResponseTypeZero(): void
	{
		$scoringSystem = new ScoringSystem(HomeworkInput::EXAPLE_DATA_0);
		$this->assertEquals($this->str_to_slug(self::RESPONSE_TYPE_0), $this->str_to_slug($scoringSystem->calculate()));
	}

	public function testResponseTypeOne(): void
	{
		$scoringSystem = new ScoringSystem(HomeworkInput::EXAPLE_DATA_1);
		$this->assertEquals($this->str_to_slug(self::RESPONSE_TYPE_1),$this->str_to_slug($scoringSystem->calculate()));
	}

	public function testResponseTypeTwo(): void
	{
		$scoringSystem = new ScoringSystem(HomeworkInput::EXAPLE_DATA_2);
		$this->assertEquals($this->str_to_slug(self::RESPONSE_TYPE_2), $this->str_to_slug($scoringSystem->calculate()));
	}

	public function testResponseTypeThree(): void
	{
		$scoringSystem = new ScoringSystem(HomeworkInput::EXAPLE_DATA_3);
		$this->assertEquals($this->str_to_slug(self::RESPONSE_TYPE_3), $this->str_to_slug($scoringSystem->calculate()));
	}
}