<?php

use \PHPUnit\Framework\TestCase;

require 'src\ScoringSystem.php';
require 'src\Controllers\AbstractBaseController.php';
require 'src\HelperTrait.php';
require 'src\Controllers\SchoolController.php';
require 'src\Models\AbstractModel.php';
require 'src\Models\School\SchoolModelInterface.php';
require 'src\Models\School\BaseSchoolModel.php';
require 'src\Models\School\SchoolModel.php';
require 'src\Models\Subject\SubjectInterface.php';
require 'src\Models\Subject\AbstractSubjectModel.php';
require 'input\HomeworkInput.php';
require 'src\Controllers\CompetitionController.php';
require 'src\Models\Competition\CompetitionModel.php';
require 'src\Models\Subject\RequiredSubjectModel.php';
require 'src\Models\Subject\OptionalSubjectModel.php';
require 'src\Models\ExtraCertificate\ExtraCertificateModel.php';
require 'src\Controllers\RequiredSubjectController.php';
require 'src\Controllers\OptionalSubjectController.php';
require 'src\Controllers\ExtraCertificatesController.php';
require 'src\Messages\Messages.php';
require 'src\Messages\ErrorMessage.php';


/**
 * Class CalculateTest
 *
 * RUN command:
 * php vendor/bin/phpunit test/School/SchoolTest.php
 */
class SchoolTest extends TestCase
{

	public function testSchoolNameInitialization(): void
	{
		$schoolController = new SchoolController();
		$schoolController->init();
		$this->assertEquals('Anglisztika', $schoolController->getSchoolModel()[1]->getPart());
	}

	public function testSchoolRequiredSubjectInitialization(): void
	{
		$schoolController = new SchoolController();
		$schoolController->init();
		$this->assertEquals('matematika', $schoolController->getSchoolModel()[0]->getRequiredSubjects()[0]);
	}
}