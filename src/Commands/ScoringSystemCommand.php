<?php

require 'src\ScoringSystem.php';
require 'src\Controllers\AbstractBaseController.php';
require 'src\HelperTrait.php';
require 'src\Controllers\SchoolController.php';
require 'src\Models\AbstractModel.php';
require 'src\Models\School\SchoolModelInterface.php';
require 'src\Models\School\BaseSchoolModel.php';
require 'src\Models\School\SchoolModel.php';
require 'src\Models\Subject\SubjectInterface.php';
require 'src\Models\Subject\AbstractSubjectModel.php';
require 'input\HomeworkInput.php';
require 'src\Controllers\CompetitionController.php';
require 'src\Models\Competition\CompetitionModel.php';
require 'src\Models\Subject\RequiredSubjectModel.php';
require 'src\Models\Subject\OptionalSubjectModel.php';
require 'src\Models\ExtraCertificate\ExtraCertificateModel.php';
require 'src\Controllers\RequiredSubjectController.php';
require 'src\Controllers\OptionalSubjectController.php';
require 'src\Controllers\ExtraCertificatesController.php';
require 'src\Messages\Messages.php';
require 'src\Messages\ErrorMessage.php';

/**
 * RUN command
 * php src/Commands/ScoringSystemCommand.php 3
 */
class ScoringSystemCommand
{
	public function main (array $argv): string
	{
		if (!isset($argv[1])) {
			$args = "0";
		}else{
			$args = $argv[1];
		}

		switch ($args) {
			case "0" : $input = HomeworkInput::EXAPLE_DATA_0; break;
			case "1" : $input = HomeworkInput::EXAPLE_DATA_1; break;
			case "2" : $input = HomeworkInput::EXAPLE_DATA_2; break;
			case "3" : $input = HomeworkInput::EXAPLE_DATA_3; break;
			default: $input = HomeworkInput::EXAPLE_DATA_0; break;
		}

		return (new ScoringSystem($input))->calculate();

	}
}

$scoringSystem = new ScoringSystemCommand();
echo $scoringSystem->main($_SERVER['argv']);