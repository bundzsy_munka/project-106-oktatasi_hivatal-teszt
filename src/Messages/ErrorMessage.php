<?php

class ErrorMessage extends Messages
{
	public const ERROR_MESSAGE_PREFIX = 'output: hiba, ';

	public const TYPE_MINIMUM_REQUIRED_SUBJECT_ERROR = 'nem lehetséges a pontszámítás a kötelező érettségi tárgyak hiánya miatt';
	public const TYPE_MINIMUM_OPTIONAL_SUBJECT_ERROR = 'nem lehetséges a pontszámítás a kötelezően választható érettségi tárgyak hiánya miatt';
	public const TYPE_MINIMUM_SCORE_ERROR = 'nem lehetséges a pontszámítás a [-SUBJECT-] tárgyból elért 20% alatti eredmény miatt';

	public function getMessage(string $inputToken = '', array $args = []): string
	{
		$this->handler($inputToken, $args);
		return parent::getMessage();
	}

	public function handler(string $inputToken, array $args): void
	{
		$message = '';
		if($inputToken === 'TYPE_MINIMUM_SCORE_ERROR') {
			$message = str_replace('[-SUBJECT-]', $args['subject'] , self::TYPE_MINIMUM_SCORE_ERROR);
		}
		if($inputToken === 'TYPE_MINIMUM_REQUIRED_SUBJECT_ERROR') {
			$message = self::TYPE_MINIMUM_REQUIRED_SUBJECT_ERROR;
		}
		if($inputToken === 'TYPE_MINIMUM_OPTIONAL_SUBJECT_ERROR') {
			$message = self::TYPE_MINIMUM_OPTIONAL_SUBJECT_ERROR;
		}
		parent::setMessage(self::ERROR_MESSAGE_PREFIX .$message);
	}
}