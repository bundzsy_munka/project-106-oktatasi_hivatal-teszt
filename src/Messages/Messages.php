<?php

class Messages
{
	private string $message;
	private string $inputToken;

	public function getMessage(): string
	{
		return $this->message;
	}

	public function setMessage(string $message): self
	{
		$this->message = $message;
		return $this;
	}

}