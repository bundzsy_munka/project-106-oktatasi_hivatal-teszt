<?php

class ExtraCertificateModel
{
	private string $category;
	private string $level;
	private string $name;

	public function getCategory (): string
	{
		return $this->category;
	}

	public function setCategory (string $category): self
	{
		$this->category = $category;
		return $this;
	}

	public function getLevel (): string
	{
		return $this->level;
	}

	public function setLevel (string $level): self
	{
		$this->level = $level;
		return $this;
	}

	public function getName (): string
	{
		return $this->name;
	}

	public function setName (string $name): self
	{
		$this->name = $name;
		return $this;
	}
	
	
}