<?php

class BaseSchoolModel implements SchoolModelInterface
{

	public const SCHOOL_ELTE = 'ELTE';
	public const SCHOOL_PPKE = 'PPKE';

	public const GROUP_IK = 'IK';
	public const GROUP_BTK = 'BTK';

	public const PART_SOFTWARE_ENGINEERING = 'Programtervező informatikus';
	public const PART_ANGLISZTIKA = 'Anglisztika';

	public string $name;
	private string $group;
	private string $part;

	/**
	 * @param array $args = [
	 *      'name' => 'ELTE',
	 *      'group' => 'IK',
	 *      'part' => 'Programtervező Informatikus'
	 *  ]
	 */
	public function __construct (array $args)
	{
		$this->name = $args['name'] ?? null;
		$this->group = $args['group'] ?? null;
		$this->part = $args['part'] ?? null;
	}

	public function getName (): string
	{
		return $this->name;
	}

	public function setName (string $name): SchoolModelInterface
	{
		$this->name = $name;
		return $this;
	}

	public function getGroup (): string
	{
		return $this->group;
	}

	public function setGroup (string $group): self
	{
		$this->group = $group;
		return $this;
	}

	public function getPart (): string
	{
		return $this->part;
	}

	public function setPart (string $part): self
	{
		$this->part = $part;
		return $this;
	}
}