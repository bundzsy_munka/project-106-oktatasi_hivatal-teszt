<?php

interface SchoolModelInterface
{
	public function setName(string $name): self;

	public function getName(): string;

	public function getGroup (): string;

	public function setGroup (string $group): self;

	public function getPart (): string;

	public function setPart (string $part): self;
}