<?php

class SchoolModel extends BaseSchoolModel
{

	private array $requiredSubjects;

	private array $optionalSubjects;

	public function getRequiredSubjects (): array
	{
		return $this->requiredSubjects;
	}

	public function setRequiredSubjects (array $requiredSubjects): self
	{
		$this->requiredSubjects = $requiredSubjects;
		return $this;
	}

	public function getOptionalSubjects (): array
	{
		return $this->optionalSubjects;
	}

	public function setOptionalSubjects (array $optionalSubjects): self
	{
		$this->optionalSubjects = $optionalSubjects;
		return $this;
	}


}