<?php

/**
 * Pályázat
 */
class CompetitionModel
{

	public const MAX_EXTRA_POINT = 100;
	public const MIN_EXTRA_POINT = 0;
	public const MAX_BASE_POINT = 400;
	public const MIN_BASE_POINT = 0;

	private BaseSchoolModel $school;
	private array $requiredSubjects;
	private array $optionalSubjects;
	private array $extraCertificates;
	private int $basePoint;
	private int $extraPoint;

	public function __construct ()
	{
		$this->basePoint = 0;
		$this->extraPoint = 0;
		$this->extraCertificates = [];
		$this->optionalSubjects = [];
		$this->requiredSubjects = [];

	}

	public function getSchool (): BaseSchoolModel
	{
		return $this->school;
	}

	public function setSchool (?BaseSchoolModel $school): self
	{
		$this->school = $school;
		return $this;
	}

	public function getRequiredSubjects (): array
	{
		return $this->requiredSubjects;
	}

	public function addRequiredSubject (RequiredSubjectModel $subject): self
	{
		$this->requiredSubjects[] = $subject;
		return $this;
	}

	public function getOptionalSubjects (): array
	{
		return $this->optionalSubjects;
	}

	public function addOptionalSubject (OptionalSubjectModel $subject): self
	{
		$this->optionalSubjects[] = $subject;
		return $this;
	}

	public function getExtraCertificates (): ?array
	{
		return $this->extraCertificates;
	}

	public function addExtraCertificate (ExtraCertificateModel $extraCertificates): self
	{
		$this->extraCertificates[] = $extraCertificates;
		return $this;
	}

	public function getBasePoint (): int
	{
		return $this->basePoint;
	}

	public function addBasePoint (int $basePoint): self
	{
		if($this->basePoint < self::MAX_BASE_POINT){
			$this->basePoint += $basePoint;
		}

		if(self::MAX_BASE_POINT <= $this->basePoint ){
			$this->basePoint = self::MAX_BASE_POINT;
		}

		return $this;
	}

	public function getExtraPoint (): int
	{
		return $this->extraPoint;
	}

	public function resetExtraPoint (int $extraPoint): self
	{
		if($extraPoint < $this->extraPoint){
			$this->extraPoint -= $extraPoint;
		}

		if($this->extraPoint <= self::MIN_EXTRA_POINT ){
			$this->extraPoint = self::MIN_EXTRA_POINT;
		}

		return $this;
	}

	public function addExtraPoint (int $extraPoint): self
	{
		if($this->extraPoint < self::MAX_EXTRA_POINT){
			$this->extraPoint += $extraPoint;
		}

		if(self::MAX_EXTRA_POINT <= $this->extraPoint ){
			$this->extraPoint = self::MAX_EXTRA_POINT;
		}

		return $this;
	}


}