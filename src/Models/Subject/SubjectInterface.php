<?php

interface SubjectInterface
{
	public function isRequiredSubject(): bool;
}