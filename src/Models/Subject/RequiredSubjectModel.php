<?php

class RequiredSubjectModel extends AbstractSubjectModel
{
	public const REQUIRED_SUBJECT = true;

	public bool $isRequired;

	public function __construct ()
	{
		$this->isRequired = self::REQUIRED_SUBJECT;
	}

	public function isRequiredSubject (): bool
	{
		return $this->isRequired;
	}
}