<?php

class OptionalSubjectModel extends AbstractSubjectModel
{
	public const REQUIRED_SUBJECT = false;

	public bool $isRequired;

	public function __construct ()
	{
		$this->isRequired = self::REQUIRED_SUBJECT;
	}

	public function isRequiredSubject (): bool
	{
		return $this->isRequired;
	}
}