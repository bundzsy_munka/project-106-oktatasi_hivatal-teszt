<?php

abstract class AbstractSubjectModel extends AbstractModel implements SubjectInterface
{
	use HelperTrait;

	public const SUBJECT_TYPE_HUN_GRAMMAR_AND_LITERATURE = 'magyar nyelv és irodalom';
	public const SUBJECT_TYPE_MATH = 'matematika';
	public const SUBJECT_TYPE_HISTORY = 'történelem';
	public const SUBJECT_TYPE_ENGLISH = 'angol nyelv';
	public const SUBJECT_TYPE_IT = 'informatika';
	public const SUBJECT_TYPE_BIOLOGY = 'biológia';
	public const SUBJECT_TYPE_CHEMISTRY = 'kémia';
	public const SUBJECT_TYPE_PHYSICS = 'fizika';
	public const SUBJECT_TYPE_FRENCH = 'francia';
	public const SUBJECT_TYPE_GERMAN = 'német';
	public const SUBJECT_TYPE_ITALIAN = 'olasz';
	public const SUBJECT_TYPE_RUSSIAN = 'orosz';
	public const SUBJECT_TYPE_SPANISH = 'spanyol';

	public const REQUIRED_SUBJECTS = [
		self::SUBJECT_TYPE_MATH,
		self::SUBJECT_TYPE_HISTORY,
		self::SUBJECT_TYPE_HUN_GRAMMAR_AND_LITERATURE
	];

	public const OPTIONAL_SUBJECTS = [
		self::SUBJECT_TYPE_ENGLISH,
		self::SUBJECT_TYPE_IT,
		self::SUBJECT_TYPE_BIOLOGY,
		self::SUBJECT_TYPE_CHEMISTRY,
		self::SUBJECT_TYPE_PHYSICS,
	];

	public const LEVEL_MEDIUM = 'közép';
	public const LEVEL_ELEVATED = 'emelt';

	public const REQUIRED_MINIMUM_OUTCOME = 20;

	public const STATUS_FAILED = 'sikeres';
	public const STATUS_SUCCESSFUL = 'sikertelen';

	private string $name;
	private string $level;
	private string $score;
	private bool $isEnoughScore;

	public function getName (): string
	{
		return $this->name;
	}

	public function setName (string $name): self
	{
		$this->name = $name;
		return $this;
	}

	public function getLevel (): string
	{
		return $this->level;
	}

	public function setLevel (string $level): self
	{
		$this->level = $level;
		return $this;
	}

	public function getScore (): string
	{
		return $this->score;
	}

	public function setScore (string $score): self
	{
		$this->score = $score;
		$this->isEnoughScore($this);
		return $this;
	}

	public function isEnoughScore ( ?AbstractSubjectModel $abstractSubjectModel = null): bool
	{
		if($abstractSubjectModel !== null){
			$this->isEnoughScore = false;
			if (self::REQUIRED_MINIMUM_OUTCOME <= (int) $this->trim_percent($abstractSubjectModel->getScore()) )
			{
				$this->isEnoughScore = true;
			}
		}

		return $this->isEnoughScore;
	}

}