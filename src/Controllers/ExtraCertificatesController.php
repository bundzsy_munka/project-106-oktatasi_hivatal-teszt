<?php

class ExtraCertificatesController extends AbstractBaseController
{
	use HelperTrait;

	public const TYPE_CATEGORY = 'Nyelvvizsga';
	public const LEVEL_SCORE_B2 = 28;
	public const LEVEL_SCORE_C1 = 40;

	public function create (array $extraCertificate): ExtraCertificateModel
	{
		$certification = new ExtraCertificateModel();
		$certification->setCategory($extraCertificate['kategoria']);
		$certification->setLevel($extraCertificate['tipus']);
		$certification->setName($extraCertificate['nyelv']);

		return $certification;
	}

	public function setExtraScoreOfExtraCertifications (CompetitionModel $competition): CompetitionModel
	{
		$args = [];

		/** @var ExtraCertificateModel $extraCertificate */
		foreach ($competition->getExtraCertificates() as $extraCertificate) {
			if ($extraCertificate->getCategory() === self::TYPE_CATEGORY) {
				$certificateSlugName = $this->str_to_slug($extraCertificate->getName());
				$currentScore = $this->getConstScoreByLevel($extraCertificate->getLevel());
				if (isset($args[$certificateSlugName])) {
					$argsScore = $this->getConstScoreByLevel($args[$certificateSlugName]->getLevel());
					if ($currentScore <= $argsScore) {
						continue;
					}
					$competition->resetExtraPoint($argsScore);
				}
				$competition->addExtraPoint($currentScore);
				$args[$this->str_to_slug($extraCertificate->getName())] = $extraCertificate;
			}
		}

		return $competition;
	}

	public function getConstScoreByLevel (string $level): int
	{
		$score = 0;
		switch ($level) {
			case 'B2':
				$score = self::LEVEL_SCORE_B2;
				break;
			case 'C1':
				$score = self::LEVEL_SCORE_C1;
				break;
		}
		return $score;
	}
}