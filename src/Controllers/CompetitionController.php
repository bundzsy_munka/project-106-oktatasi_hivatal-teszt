<?php

class CompetitionController extends AbstractBaseController
{

	private CompetitionModel $competition;

	public function getCompetition (): CompetitionModel
	{
		return $this->competition;
	}

	public function setCompetition (CompetitionModel $competition): self
	{
		$this->competition = $competition;
		return $this;
	}

	/**
	 * @param array $args = [
	 *                    'valasztott-szak' => [
	 *                    'egyetem' => 'ELTE',
	 *                    'kar' => 'IK',
	 *                    'szak' => 'Programtervező informatikus'
	 *                    ],
	 *                    'erettsegi-eredmenyek' => [[
	 *                    'nev' => 'matematika',
	 *                    'tipus' => 'emelt',
	 *                    'eredmeny' => '90%'
	 *                    ]],
	 *                    'tobbletpontok' => [[
	 *                    'kategoria' => 'Nyelvvizsga',
	 *                    'tipus' => 'B2',
	 *                    'nyelv' => 'angol',
	 *                    ]]
	 */
	public function create (array $args, array $baseSchoolModels): ?CompetitionModel
	{
		$competition = new CompetitionModel();

		$competition = $this->addSchool($competition, $args['valasztott-szak'], $baseSchoolModels);
		$competition = $this->addSubjects($competition, $args['erettsegi-eredmenyek']);
		$competition = $this->addExtraCertificates($competition, $args['tobbletpontok']);

		$this->competition = $competition;
		return $this->competition;
	}

	/**
	 * @param array $subjects = [[
	 *                        'nev' => 'informatika',
	 *                        'tipus' => 'közép',
	 *                        'eredmeny' => '95%',
	 *                        ]]
	 */
	public function addSubjects (CompetitionModel $competition, array $subjects): ?CompetitionModel
	{
		foreach ($subjects as $subject) {
			if (in_array($subject['nev'], AbstractSubjectModel::REQUIRED_SUBJECTS, false)) {
				$requireSubjectController = new RequiredSubjectController();
				$requireSubject = $requireSubjectController->create($subject);
				$competition->addRequiredSubject($requireSubject);
			}
			if (in_array($subject['nev'], AbstractSubjectModel::OPTIONAL_SUBJECTS, false)) {
				$optionalSubjectController = new OptionalSubjectController();
				$optionalSubject = $optionalSubjectController->create($subject);
				$competition->addOptionalSubject($optionalSubject);
			}
		}
		$this->competition = $competition;

		return $competition;
	}

	/**
	 * @param array $school = [
	 *                      'egyetem' => 'ELTE',
	 *                      'kar' => 'IK',
	 *                      'szak' => 'Programtervező informatikus',
	 *                      ]
	 */
	public function addSchool (CompetitionModel $competition, array $school, array $baseSchoolModels): CompetitionModel
	{
		$args = [
			'name' => $school['egyetem'],
			'group' => $school['kar'],
			'part' => $school['szak'],
		];

		$schoolData = null;
		foreach ($baseSchoolModels as $baseSchoolModel) {
			if ($args['name'] === $baseSchoolModel->getName() &&
				$args['group'] === $baseSchoolModel->getGroup() &&
				$args['part'] === $baseSchoolModel->getPart()) {
				$schoolData = $baseSchoolModel;
			}
		}
		$this->competition = $competition;

		return $competition->setSchool($schoolData);
	}

	/**
	 * @param array $extraCertificates = [
	 *                                 'kategoria' => 'Nyelvvizsga',
	 *                                 'tipus' => 'B2',
	 *                                 'nyelv' => 'angol',
	 *                                 ]
	 */
	private function addExtraCertificates (CompetitionModel $competition, array $extraCertificates): CompetitionModel
	{
		foreach ($extraCertificates as $extraCertificate) {
			$extraCertificateController = new ExtraCertificatesController();
			$certificate = $extraCertificateController->create($extraCertificate);
			$competition->addExtraCertificate($certificate);
		}
		$this->competition = $competition;

		return $competition;
	}


}