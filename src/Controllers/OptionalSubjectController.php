<?php

class OptionalSubjectController extends AbstractBaseController
{
	use HelperTrait;

	public function create (array $optionalSubject): OptionalSubjectModel
	{
		$subject = new OptionalSubjectModel();
		$subject->setName($optionalSubject['nev']);
		$subject->setLevel($optionalSubject['tipus']);
		$subject->setScore($optionalSubject['eredmeny']);

		return $subject;
	}

	public function setOptionalScoreOfOptionalSubjects (CompetitionModel $competition)
	{
		$maxOptionalSubjectScore = $userExtraOptionalScore = 0;
		$acceptedSubject = false;

		foreach ($competition->getSchool()->getOptionalSubjects() as $schoolOptionalSubject){
			$schoolSubjecSlug = $this->str_to_slug($schoolOptionalSubject);

			/** @var OptionalSubjectModel $userOptionalSubject */
			foreach ($competition->getOptionalSubjects() as $userOptionalSubject){
				if (!$userOptionalSubject->isEnoughScore() || $schoolSubjecSlug !== $this->str_to_slug($userOptionalSubject->getName())){
					continue;
				}

				$acceptedSubject = true;
				$userExtraOptionalScore = $this->trim_percent($userOptionalSubject->getLevel()) === $this->trim_percent(AbstractSubjectModel::LEVEL_ELEVATED) ? 50 : 0;
				$userOptionalScore = (int)$this->trim_percent($userOptionalSubject->getScore()) + $userExtraOptionalScore;

				$maxOptionalSubjectScore = ($maxOptionalSubjectScore < $userOptionalScore ) ? (int) $userOptionalScore : $maxOptionalSubjectScore;
			}

		}
		if (!$acceptedSubject) {
			return (new ErrorMessage())->getMessage('TYPE_MINIMUM_REQUIRED_SUBJECT_ERROR');
		}

		$competition->addBasePoint(($maxOptionalSubjectScore - $userExtraOptionalScore) * 2 );
		$competition->addExtraPoint($userExtraOptionalScore);

		return $competition;
	}
}