<?php

class SchoolController extends AbstractBaseController
{
	private const SCHOOL_DATA = [
		[
			'name' => BaseSchoolModel::SCHOOL_ELTE,
			'group' => BaseSchoolModel::GROUP_IK,
			'part' => BaseSchoolModel::PART_SOFTWARE_ENGINEERING,
			'required_subjects' => [
				AbstractSubjectModel::SUBJECT_TYPE_MATH
			],
			'optional_subjects' => [
				AbstractSubjectModel::SUBJECT_TYPE_BIOLOGY,
				AbstractSubjectModel::SUBJECT_TYPE_CHEMISTRY,
				AbstractSubjectModel::SUBJECT_TYPE_IT,
				AbstractSubjectModel::SUBJECT_TYPE_PHYSICS,
			]
		],
		[
			'name' => BaseSchoolModel::SCHOOL_PPKE,
			'group' => BaseSchoolModel::GROUP_BTK,
			'part' => BaseSchoolModel::PART_ANGLISZTIKA,
			'required_subjects' => [
				AbstractSubjectModel::SUBJECT_TYPE_ENGLISH
			],
			'optional_subjects' => [
				AbstractSubjectModel::SUBJECT_TYPE_FRENCH,
				AbstractSubjectModel::SUBJECT_TYPE_GERMAN,
				AbstractSubjectModel::SUBJECT_TYPE_ITALIAN,
				AbstractSubjectModel::SUBJECT_TYPE_RUSSIAN,
				AbstractSubjectModel::SUBJECT_TYPE_SPANISH,
				AbstractSubjectModel::SUBJECT_TYPE_HISTORY,
			]
		]
	];

	public array $schoolModel;

	public function getSchoolModel (): array
	{
		return $this->schoolModel;
	}

	public function setSchoolModel (array $schoolModel): self
	{
		$this->schoolModel = $schoolModel;
		return $this;
	}

	public function init(): void
	{
		/** @var SchoolModel $schoolData */
		foreach (self::SCHOOL_DATA as $schoolData){
			$school = $this->create($schoolData);
			$school->setRequiredSubjects($schoolData['required_subjects']);
			$school->setOptionalSubjects($schoolData['optional_subjects']);
			$this->schoolModel[] = $school;
		}
	}

	public function create(array $school): SchoolModelInterface
	{
		$args = [
			'name' => $school['name'],
			'group' => $school['group'],
			'part' => $school['part']
		];

		return new SchoolModel($args);
	}
}