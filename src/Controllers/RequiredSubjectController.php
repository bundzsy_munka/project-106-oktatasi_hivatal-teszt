<?php

class RequiredSubjectController extends AbstractBaseController
{

	use HelperTrait;

	public function create (array $requiredSubject): RequiredSubjectModel
	{
		$subject = new RequiredSubjectModel();
		$subject->setName($requiredSubject['nev']);
		$subject->setLevel($requiredSubject['tipus']);
		$subject->setScore($requiredSubject['eredmeny']);

		return $subject;
	}

	public function setRequiredScoreOfRequiredSubjects (CompetitionModel $competition)  // visszatérési érték php 8.0 nál nagyobb esetén [CompetitionModel|string]
	{
		$maxRequiredSubjectScore = $userExtraRequiredScore = 0;

		foreach (AbstractSubjectModel::REQUIRED_SUBJECTS as $requiredSubject) {
			$schoolSubjectSlug = $this->str_to_slug($requiredSubject);
			$acceptedSubject = false;

			/** @var RequiredSubjectModel $userRequiredSubject */
			foreach ($competition->getRequiredSubjects() as $userRequiredSubject) {

				if (!$userRequiredSubject->isEnoughScore() ){
					return (new ErrorMessage())->getMessage('TYPE_MINIMUM_SCORE_ERROR', ['subject' => $userRequiredSubject->getName()]);
				}
				if ($schoolSubjectSlug !== $this->str_to_slug($userRequiredSubject->getName())){
					continue;
				}

				$acceptedSubject = true;

				if (in_array( $userRequiredSubject->getName() , $competition->getSchool()->getRequiredSubjects())){
					$userExtraRequiredScore = $this->trim_percent($userRequiredSubject->getLevel()) === $this->trim_percent(AbstractSubjectModel::LEVEL_ELEVATED) ? 50 : 0;
					$maxRequiredSubjectScore = (int)$this->trim_percent($userRequiredSubject->getScore()) ;
				}
			}

			if (!$acceptedSubject) {
				return (new ErrorMessage())->getMessage('TYPE_MINIMUM_REQUIRED_SUBJECT_ERROR');
			}

		}

		$competition->addBasePoint($maxRequiredSubjectScore * 2);
		$competition->addExtraPoint($userExtraRequiredScore);

		return $competition;

	}
}