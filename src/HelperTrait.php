<?php

trait HelperTrait
{
	public function str_to_slug(string $text, string $divider = '-'): string
	{
		$text = preg_replace('~[^\pL\d]+~u', $divider, $text);
		$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
		$text = preg_replace('~[^-\w]+~', '', $text);
		$text = trim($text, $divider);
		$text = preg_replace('~-+~', $divider, $text);
		$text = strtolower($text);

		if (empty($text)) {
			return 'n-a';
		}

		return $text;
	}

	public function trim_percent(string $args){
		return trim( str_replace('%', '', $args));
	}
}