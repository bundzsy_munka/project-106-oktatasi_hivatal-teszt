<?php

class ScoringSystem
{
	public SchoolController $schoolController;
	public CompetitionModel $competition;

	public function __construct (array $exampleData = HomeworkInput::EXAPLE_DATA_3 )
	{
		$this->schoolController = new SchoolController();
		$this->schoolController->init();

		$this->competition  = (new CompetitionController())->create($exampleData, $this->schoolController->getSchoolModel());
	}

	public function calculate(): string
	{
		$requiredSubjectController = new RequiredSubjectController();
		$competition = $requiredSubjectController->setRequiredScoreOfRequiredSubjects($this->competition);
		if (is_string($competition)){
			return $competition;
		}
		$this->competition = $competition;

		$optionalSubjectController = new OptionalSubjectController();
		$competition = $optionalSubjectController-> setOptionalScoreOfOptionalSubjects($this->competition);
		if (is_string($competition)){
			return $competition;
		}
		$this->competition = $competition;

		$extraCertificationController = new ExtraCertificatesController();
		$this->competition = $extraCertificationController->setExtraScoreOfExtraCertifications($this->competition);

		return 'output: '.($this->competition->getExtraPoint() + $this->competition->getBasePoint()). ' ('. $this->competition->getBasePoint() .' alappont + '
		.$this->competition->getExtraPoint().' többletpont)';
	}
}

